#Trabalho Final  

### Câmara refrigerada  

#Descrição do Projeto 
 
O projeto teve o intuito de criar uma câmara refrigerada que mostrasse a temperatura da câmara em um display e possua uma porta automática, além do sistema de refrigeração automático¹.
#
O projeto câmara de refrigeração é espécie de miniatura de câmaras de refrigeração que são utilizadas hoje em dia. Essas câmaras são altamente utilizadas em hospitais, frigoríficos, laboratórios de pesquisas entre outros estabelecimentos que necessitem controlar o acesso a determinado material.

#
¹Em um primeiro momento não haveria o controle de temperatura, no entanto, posteriormente foi possível acrescentar essa função ao sistema.  
 
#Plataforma
 
Arduino 

#Material 

#
* Para execução do projeto em principio serão necessários:
# 
* Sensor de temperatura LM35; 
#
* Display LDC de 16x2 seguimentos;
# 
* Leitor RFID; 
#
* Eletroímã;
# 
* Sistema de refrigeração²;
#
* Fonte 12V e 05V; 
#
* 01Rele;
#
* 02 Transistores³;
#
* 02 Resistores³;
#
* 01 Diodo³;
#
* 01 Capacitor 1uf³.

²O sistema de refrigeração será controlado por meio de acionamento (liga/desliga) a Placa Peltier, responsável pelo gradiente de calor da câmara de refrigeração. O Sistema de refrigeração conta com duas ventoinhas uma maior(externa) e outra menor(interna) para dissipação do gradiente de temperatura.
#
³Esses componentes foram acrescentados com o decorrer da montagem do projeto uma vez que os mesmos variavam de acordo conforme foram aparecendo a demanda de integração do circuito.  
  
 
#Funcionamento 

O sistema foi projetado para exibir a temperatura interna da câmara de refrigeração em um display LCD 16x2, esse display ainda exibirá uma mensagem pedindo para que se aproxime um cartão ao módulo RFID, que abrirá a porta se o cartão estiver cadastrado através do acionamento de um eletroímã que criará uma campo magnético que repelirá o imã permanente contido na porta e esta por sua vez será destravada. O eletroímã ficará acionado por três segundo, logo após será desativado. A temperatura interna ainda será controlada pelo acionamento (liga/desliga) da placa Peltier. 

#Avanços na execução do Projeto

###Primeira Parte do código.###

#
Esta primeira etapa contou apenas com o implementação do código base que controlava o LCD e exibia a temperatura medida pelo sensor de LM35⁴.
#
O esquema está da simulação consta no seguinte link:
#
[https://circuits.io/circuits/3715760-parte-um-de-implementacao-do-codigo-da-camara-refrigerada](https://circuits.io/circuits/3715760-parte-um-de-implementacao-do-codigo-da-camara-refrigerada)   
#
O código ainda em versão inicial:  
#
  

```
#!arduino



#include <LiquidCrystal.h>
LiquidCrystal lcd(2,3,4,5,6,7);
float entrada;
float T;
#include <SPI.h>
#include <LiquidCrystal.h>
 
#define SS_PIN 10
#define RST_PIN 9

 
 
char st[20];
 
void setup() 
{
     Serial.begin(9600);
     lcd.begin(16,2);
     lcd.home();
     lcd.clear();
     lcd.print( T);
  	lcd.setCursor(0,1);
	lcd.print("acesso liberado");
     

}

void loop() 
{
lcd.home();
lcd.clear();
lcd.setCursor(0,1);
lcd.print("acesso liberado");
entrada = analogRead(A0);
T = (entrada*0.48573444);
lcd.setCursor(0,0);
lcd.print("Temp: ");
lcd.print(T);
lcd.print("C");

delay(2000);
}
```
⁴A referencia utilizada para criar o código foi: [https://circuits.io/circuits/2919000-display-lcd-16x2-e-sensor-de-temperatura-lm35](https://circuits.io/circuits/2919000-display-lcd-16x2-e-sensor-de-temperatura-lm35)

O programa⁵ foi modificado varias vezes afim de adequar um melhor funcionamento câmara de refrigeração sua ultima atualização ficou dessa forma:

```
#!arduino

#include <SPI.h>
#include <MFRC522.h>
#include <LiquidCrystal.h>
#include <math.h>

#define SS_PIN 10
#define RST_PIN 9
#define PINTO A5
#define PELTIER A3

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

LiquidCrystal lcd(6, 7, 5, 4, 3, 2); 

//sensor grove
const int B=4275;                 // B value of the thermistor
float R;
const int grove = A2;     // Grove - Temperature Sensor connect to A5
int in_grove;
float T_grove;
int temperatura;
int bin;

//lm35
int entrada;
float T;
float temp;
int i=0;


char st[20];
 
void setup() 
{
  Serial.begin(9600);   // Inicia a serial
  analogReference(INTERNAL);
  SPI.begin();      // Inicia  SPI bus
  mfrc522.PCD_Init();   // Inicia MFRC522
  Serial.println("Aproxime o seu cartao do leitor...");
  Serial.println();
  //Define o número de colunas e linhas do LCD:  
  lcd.begin(16, 2);  
  mensageminicial();
  tempLCD();    
  pinMode(PINTO,OUTPUT);
  pinMode(PELTIER,OUTPUT);
}
 
void loop() 
{
  delay(100);
  i++;
  if(i==20){               //i = frequencia que a temperatura é atualizada (em segundos)  * 10
      i=0;
      tempLCD();    
      
      temperatura = (int) T;
      Serial.println(temperatura);
      if(temperatura<15){
        Serial.println("desliga geladeira");
        bin = 0;
      }
      if(temperatura>=25){
        Serial.println("liga geladeira");
        bin=1;
      }
        Serial.println(bin);
        digitalWrite(PELTIER,bin);//PNP
  }

  
  
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent()) 
  {
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) 
  {
    return;
  }
  //Mostra UID na serial
  Serial.print("UID da tag :");
  String conteudo= "";
  byte letra;
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
     Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
     Serial.print(mfrc522.uid.uidByte[i], HEX);
     conteudo.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
     conteudo.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  Serial.println();
  Serial.print("Mensagem : ");
  conteudo.toUpperCase();
  if (conteudo.substring(1) == "86 E7 2C 5E") //UID 1 - Chaveiro
  {
    Serial.println("Ola evandro!");
    Serial.println();
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("ae pourra!");
    lcd.setCursor(0,1);
    lcd.print("naaaaice!");
    delay(1000);
    mensageminicial();
    tempLCD();    
    //roda motor
  }
 
  if (conteudo.substring(1) == "83 4B 9F D5") //UID 2 - Cartao
  {
    Serial.println("Ola Cartao !");
    Serial.println();
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("olar leila");
    lcd.setCursor(0,1);
    lcd.print("Acesso legal !");
    digitalWrite(PINTO,HIGH);
    delay(1000);
    digitalWrite(PINTO,LOW);
    mensageminicial();
    tempLCD();    
  }
} 
 
void mensageminicial()
{
  lcd.clear();
  lcd.print("Aproxime o card");  
}

void tempLCD(){
  
  //sensor grove
  in_grove = analogRead(grove);
  R = 1023.0/((float)in_grove)-1.0;
  R = 100000.0*R;
  T_grove = 1.0/(log(R/100000.0)/B+1/298.15)-273.15;//convert to temperature via datasheet ;
  Serial.print("Temperatuta Grove: ");
  Serial.print(T_grove);
  Serial.print("C");
  Serial.println();

  //lm35
  entrada = analogRead(A0);
  T = entrada/9.31;
  Serial.print("Temperatuta LM35: ");
  Serial.print(T);
  Serial.print("C");
  Serial.println();
  lcd.clear();
  mensageminicial();
  lcd.setCursor(0,1);
  lcd.print("Temp:  ");
  lcd.print(T);  
  lcd.setCursor(12,1);
  lcd.print("  C");  
}


```
⁵Essa parte final do programa em grande parte baseada no código disponível no blog:
[http://blog.filipeflop.com/wireless/controle-acesso-leitor-rfid-arduino.html](http://blog.filipeflop.com/wireless/controle-acesso-leitor-rfid-arduino.html)  

### Esquemático do Projeto  ###
#
O esquemático inicial contou com a implementação do LDC 16x2 e o sensor de temperatura, alem do circuito do sistema de refrigeração, posteriormente foi acrescentado o eletroímã e por fim foi acrescentado o controle de temperatura, parte mais sensível. A mediada que se avançava a implementação foram encontrados alguns desafios que provocou algumas mudanças. Esses desafios ao longo da execução foram:
#
1°Na implementação do LCD 16x2 foi identificado a necessidade de um potenciômetro, pra controle do contraste do LDC;
#
2°Foi identificado ruido na leitura da temperatura feita pelo LM35, assim após varias pesquisas a solução para esse problema foi o incremento de um capacitor 1uF. Esse valor de capacitância inicialmente era de 47uF, no entanto por fins didáticos, o capacitor foi trocado por um de 3.3uF e por fim pelo capacitor de 1uF, valor esse que sanou os ruídos existentes na leitura inicial do LM35;        
#
3°A construção do eletroímã contou com alguns percalços, uma vez que a maioria dos modelos encontrados na internet mostrava a montagem de forma simplória que quando seguida não resultava em um eletroímã com campo magnético suficientemente forte. O problemas nesses tutoriais é que os mesmos não levavam em consideração o principio físico da lei de Biot-Savart que diz a respeito da orientação da corrente passando pelo fio enrolado que por sua vez gera o campo magnético. Assim ao se seguir esses tutoriais pela internet deve se ter em mente que não se deve enrolar o fio de cobre esmaltado em um sentido e voltar enrolando em outro, conforme erroneamente a maioria dos tutoriais levam as pessoas a fazer, pois isso faz com que a orientação da corrente de camada a camada seja trocado, criando assim um campo magnético baixo. Desse modo a melhor forma ao se fazer um eletroímã é enrolar as camadas de fio de cobre esmaltado com cuidado para que a orientação da corrente não se inverta de uma camada a outra, ou seja, os fios devem ser enrolados apenas em um sentido para que o campo magnético seja forte o suficiente.
#
4° Inicialmente tentou-se fazer o controle de temperatura da câmara de refrigeração por meio da implementação de um simples transistor, no entanto essa abordagem inicial teve muito sucesso uma vez que acionar a placa de Peltier por meio do transistor exige aproximadamente 6 amperes, valor esse alto de mais para um transistor suportar o que causa um super aquecimento do mesmo. A melhor abordagem encontrada nesse sentido foi a adoção de um um rele no circuito para acionamento da Placa de Peltier. Essa abordagem também pode ser adotada no acionamento do eletroímã, mas a mesma não foi adotada, pois o eletroímã é acionado apenas por três segundos o que impede a sobrecarga no transistor.

Imagem do Esquemático Final do Projeto:
![Novo esquematico..jpg](https://bitbucket.org/repo/RbKqbz/images/3986791725-Novo%20esquematico..jpg)

O Link para o esquemático final do projeto consta abaixo:               
[http://www.schematics.com/project/esquem%C3%A1tico-do-projeto-c%C3%A2mara-de-refrigera%C3%A7%C3%A3o-47464/](http://www.schematics.com/project/esquem%C3%A1tico-do-projeto-c%C3%A2mara-de-refrigera%C3%A7%C3%A3o-47464/)
#
# Montagem da Câmara #
#
As imagens a seguir mostram o processo de montagem da camará de refrigeração do inicio até sua conclusão.  
#
Imagem da montagem do projeto.
**Montagem da parte externa e elétrica do do sistema de refrigeração**
![20170124_160813.jpg](https://bitbucket.org/repo/RbKqbz/images/7966613-20170124_160813.jpg) 
![20170124_160508.jpg](https://bitbucket.org/repo/RbKqbz/images/3578750948-20170124_160508.jpg)
![20170124_160904.jpg](https://bitbucket.org/repo/RbKqbz/images/3771355061-20170124_160904.jpg)
![20170126_193204.jpg](https://bitbucket.org/repo/RbKqbz/images/291805345-20170126_193204.jpg)
![20170126_193222.jpg](https://bitbucket.org/repo/RbKqbz/images/1178501864-20170126_193222.jpg)

#
**Parte elétrica**    
#
![eletric.jpg](https://bitbucket.org/repo/RbKqbz/images/1134241834-eletric.jpg)
![Foto01.jpg](https://bitbucket.org/repo/RbKqbz/images/2576850679-Foto01.jpg)
![Foto02.jpg](https://bitbucket.org/repo/RbKqbz/images/886800617-Foto02.jpg)
![Foto03.jpg](https://bitbucket.org/repo/RbKqbz/images/3163577951-Foto03.jpg)
#
**Câmara de refrigeração funcionando**
![FOTOFINAL01.jpg](https://bitbucket.org/repo/RbKqbz/images/564487213-FOTOFINAL01.jpg)
![FOTOFINAL2.jpg](https://bitbucket.org/repo/RbKqbz/images/183446123-FOTOFINAL2.jpg)
![FOTOFINAL3.jpg](https://bitbucket.org/repo/RbKqbz/images/3883843-FOTOFINAL3.jpg)
 ![func.jpg](https://bitbucket.org/repo/RbKqbz/images/1138550769-func.jpg)